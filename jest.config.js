// 单元测试jest配置
module.exports = {
  // 依次找 js、jsx、json、vue 后缀的文件
  moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
  // 使用 vue-jest 帮助测试 .vue 文件
  // 遇到 css 等转为字符串 不作测试
  // 遇到 js jsx 等转成 es5
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    '^.+\\.jsx?$': 'babel-jest'
  },
  // 统计覆盖信息时需要忽略的文件
  coveragePathIgnorePatterns: [
    'src/assets/',
    '/node_modules/'
  ],
  // 转换时需要忽略的文件
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs)$'
  ],
  // 模块的映射 @ 开头到根目录下寻找
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  // snapshot 怎么去存储
  snapshotSerializers: ['jest-serializer-vue'],
  // npm run test:unit 时到哪些目录下去找 测试 文件
  // testMatch: [
  //   '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
  // ],
  testMatch: [ //  匹配测试用例的文件
    // '**/tests/unit/**/**.spec.js'
    '**/tests/unit/components/layout/index.spec.js',
   // '**/tests/unit/components/module/systemalarm/**.spec.js'
  ],
  // 指定需要检查覆盖率的文件
  collectCoverageFrom: ['!src/assets/**/*.{js,png}', 'src/components/**/*.{js,vue}',
    '!src/components/module/devicemanage/*.{js,vue}', '!src/components/module/fgapconfig/*.{js,vue}',
    '!src/components/login/Login1.vue', '!src/components/login/Login2.vue', '!src/components/login/Login3.vue',
],
  // 生成测试覆盖率文件
  coverageDirectory: '<rootDir>/tests/unit/coverage',
  // 是否收集测试时的覆盖率信息
  'collectCoverage': true,
  'coverageReporters': [
    'lcov',
    'text-summary'
  ],
  // 两个帮助使用 jest 的插件 过滤 测试名/文件名 来过滤测试用例
  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname'
  ],
  // 模拟的浏览器的地址是什么
  testURL: 'http://localhost/',
  // 运行一些代码以配置或设置测试环境的模块的路径列表
  setupFiles: ['./setup-jest.js'],
  testTimeout: 130000
  // jest.setTimeout(30000);
}
