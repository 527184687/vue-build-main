import $ from 'jquery';
import Vue from 'vue';
// import vueTouch from 'kim-vue-touch'
global.$ = global.jQuery = $;
global.Vue = global.Vue = Vue;
// global.vueTouch = global.vueTouch = vueTouch;

global.APIObj = window.APIObj = {
  apiUrl: 'http://192.168.32.47:8802',
  projectName: '<%= process.env.VUE_APP_BASE_PROJECTNAME %>'
}

