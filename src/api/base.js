/**
 * api请求处理
 */
import axiosInstance from '@/lib/request';
var BaseApi = {
  list(params) {
    var promise = axiosInstance({
      url: '/menu/list',
      method: 'get',
      params,
    })
    return promise
  },
  checkVerify(params) {
    var promise = new Promise(function(resolve, reject) {
      resolve({
        data: {
          code: 200
        }
      });
    })
    return promise;
  },
  login(params) {
    var promise = new Promise(function(resolve, reject) {
      resolve({
        data: {
          code: 200,
          data: params
        }
      });
    })
    return promise;
  },
  getCertificateStatus(params) {
    var promise = new Promise(function(resolve, reject) {
      resolve({
        data: {
          code: 200,
          data: params
        }
      });
    })
    return promise;
  }
}


export default BaseApi;
