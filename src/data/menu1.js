export default [
  {
    id: '2',
    menuName: '数据导入',
    icon: 'menu-monitor.png',
    path: 'dataexchange',
    code: '',
    children: [
      {
        id: '3',
        menuName: '任务管理',
        icon: null,
        path: 'taskmanage',
        code: '',
      },
      {
        id: '4',
        menuName: '资源管理',
        icon: null,
        path: 'resourcemanage',
        code: '',
      },
      {
        id: '5',
        menuName: '数据容错',
        icon: null,
        path: 'datafaulttolerant',
        code: '',
      }
    ],
  },
  {
    id: '7',
    menuName: '集控管理',
    icon: 'menu-alarm.png',
    path: 'centralizedcontrolmanage',
    code: '',
    children: [
      {
        menuName: '设备管理',
        icon: '',
        path: 'devicemanage',
        code: '',
        children: [],
      },
      {
        menuName: '设备状态',
        icon: '',
        path: 'devicestatus',
        code: '',
        children: [],
      },
      {
        menuName: '集控配置',
        icon: '',
        path: 'centralizedcontrolconfig',
        code: '',
        children: [],
      },
    ],
  },
  {
    id: '10',
    menuName: '统计分析',
    icon: 'menu-archives.png',
    path: 'statisticanalysis',
    code: null,
    children: [
      {
        menuName: '数据统计',
        icon: null,
        path: 'datastatistic',
        code: '1023',
      },
      {
        menuName: '数据分析',
        icon: null,
        path: 'dataanalysis',
        code: '1024',
      }
    ]
  }, {
    id: '11',
    menuName: '用户管理',
    icon: 'menu-archives.png',
    path: 'usermanage',
    code: null,
    children: [
      {
        menuName: '用户配置',
        icon: null,
        path: 'userconfig',
        code: '1023',
      }
    ]
  },
  {
    id: '17',
    menuName: '控制面板',
    icon: 'menu-model.png',
    path: 'controlpanel',
    code: '',
    children: [
      {
        menuName: '日志配置',
        icon: '',
        path: 'logmanage',
        code: '1028',
        children: [],
      },
      {
        menuName: '网络配置',
        icon: '',
        path: 'baseconfig',
        code: '1028',
        children: [],
      },
      {
        menuName: '备份还原',
        icon: '',
        path: 'backuprestore',
        code: '1028',
        children: [],
      },
      {
        menuName: '报警策略',
        icon: '',
        path: 'alarmstrategy',
        code: '1028',
        children: [],
      },
      {
        menuName: '系统管理',
        icon: '',
        path: 'systemmanage',
        code: '1028',
        children: [],
      },
      {
        menuName: '产品授权',
        icon: '',
        path: 'license',
        code: '1028',
        children: [],
      },
    ],
  }
];
