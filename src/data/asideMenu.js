export default [
  {
    id: '2',
    menuName: '队列数据管理',
    icon: 'menu-monitor.png',
    path: 'dlsj',
    code: '',
    disabled: true,
    children: [
      {
        id: '21',
        menuName: 'DNA队列',
        icon: null,
        active: true,
        shortcut: true,
        path: 'dnadl',
        code: '',
      },
      {
        id: '22',
        menuName: '图像队列',
        icon: null,
        path: 'txdl',
        code: '',
      },
      {
        id: '23',
        menuName: '指纹队列',
        icon: null,
        shortcut: true,
        path: 'zwdl',
        code: '',
      }
    ],
  },
  {
    id: '3',
    menuName: '对比结果管理',
    icon: 'menu-monitor.png',
    path: 'dbjg',
    code: '',
    disabled: true,
    children: [
      {
        id: '31',
        menuName: '多元对比结果',
        icon: null,
        shortcut: true,
        path: 'dydb',
        code: '',
      },
      {
        id: '32',
        menuName: '单列对比结果',
        icon: null,
        path: 'dldb',
        code: '',
      }
    ],
  }
];
