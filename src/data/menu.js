export default [
  {
    id: '2',
    menuName: '设备管理',
    icon: 'menu-monitor.png',
    path: '',
    disabled: false,
    code: '',
    children: [
      {
        id: '11',
        menuName: '设备列表',
        icon: null,
        active: true,
        shortcut: true,
        path: 'device',
        code: '',
      }
    ],
  },
  {
    id: '3',
    menuName: '日志管理',
    icon: 'menu-monitor.png',
    path: '',
    code: '',
    children: [
      {
        id: '22',
        menuName: '操作日志',
        icon: null,
        shortcut: true,
        path: 'operatelog',
        code: '',
      }
    ],
  }
];
