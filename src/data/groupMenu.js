export default [
  {
    id: '00',
    menuName: '机房管理系统',
    icon: '',
    path: '',
    disabled: false,
    code: '',
    children: [
      {
        id: '01',
        menuName: '首页',
        icon: null,
        shortcut: true,
        active: true,
        path: 'page',
        code: '',
      },
      {
        id: '02',
        menuName: '设备管理',
        icon: null,
        path: 'device',
        shortcut: true,
        disabled: true,
        code: '',
        children: [
          {
            id: '021',
            menuName: '新增',
            icon: null,
            shortcut: true,
            hide: true,
            path: 'adddevice',
            code: '',
          },
          {
            id: '021',
            menuName: '修改',
            icon: null,
            shortcut: true,
            hide: true,
            path: 'editordevice',
            code: '',
          }
        ]
      }
    ],
  }
];
