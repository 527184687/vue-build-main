// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
// import 'element-ui/lib/theme-chalk/index.css';
import Vuex from 'vuex';
/* polyfill */
import 'babel-polyfill';
import promise from 'es6-promise';
import store from '@/store/index';
// import ElementUI from 'element-ui';
import confirm from '@/util/confirm'
// ie9input框兼容性
import oninputPolyfill from 'ie9-oninput-polyfill'
import scroll from 'vue-seamless-scroll';
import DictionaryCode from '@/util/dictionaryCode';
import Filter from '@/util/filter';


import '@/main.css';
import '@/anim.css';


const styleDefine = localStorage.getItem('GLOBAL_THEME');
async function importCss() {
  console.info(styleDefine)
  if (styleDefine === 'default') {
    import('@/c-element.css');
  } else if (styleDefine === 'home') {
    // import('@/sip.css');
    // import('@/sip-element.css');
  } else {
    import('@/c-element.css');
  }
}
importCss();

import axiosInstance from '@/lib/request';
import api from '@/api/base';
import '@/lib/scroll/d3.scroll.css'
import D3Scroll from '@/lib/scroll/d3.scroll.js'
import qs from 'qs';

// ---------------------加载模块
import RemoteModule from '@/util/remoteModule'


if (typeof (ELEMENT)!=='undefined') {
  for (let k in ELEMENT) {
    window[k] = global[k] = ELEMENT[k]
  }
}
let isMobile = navigator.userAgent.toLowerCase().match(/(ipod|ipad|iphone|android|coolpad|mmp|smartphone|midp|wap|xoom|symbian|j2me|  blackberry|wince)/i) != null;
window.isMobile = global.isMobile = isMobile;
window.Vue = global.Vue = Vue;
window.d3 = global.d3 = d3;
window.D3Scroll = global.D3Scroll = D3Scroll;

window.$ = global.$ = $;
Vue.prototype.$confirm = confirm;
Vue.prototype.$api = api;
Vue.prototype.$qs = qs;
Vue.prototype.$axiosInstance = axiosInstance;
Vue.config.productionTip = false;
import { message } from '@/lib/single-element-message.js';
window.$ready = false;
window._components = {};
if (!window.console) {
  window.console = {};
  var funcs = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml',
    'error', 'exception', 'group', 'groupCollapsed', 'groupEnd',
    'info', 'log', 'markTimeline', 'profile', 'profileEnd',
    'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
  for (var i = 0, l = funcs.length; i < l; i++) {
    var func = funcs[i];
    window.console[func] = function() {
    };
  }
}
Vue.use(oninputPolyfill)
promise.polyfill();
Vue.use(DictionaryCode);
Vue.use(Filter);
// Vue.use(ElementUI);
Vue.use(Vuex);
Vue.use(scroll);
Vue.prototype.$message = message;
Vue.prototype.$findByName = function(targetName) {
  function f(children, n) {
    for (let i = 0; i < children.length; i++) {
      let c = children[i];
      if (c.$vnode.tag.includes(n)) {
        return c;
      }
      if (c.$children) {
        return  f(c.$children, n);
      }
    }
  }
  return f(this.$root.$children, targetName);
};
Vue.prototype.$findParents = function(targetName) {
  if (!this.$parent) return;
  if (this.$vnode.tag.includes(targetName)) {
    return this
  } else {
    return this.$findParents.call(this.$parent, targetName)
  }
};

// 自定义指令
Vue.directive('filterSpecialChar', {
  update(el, { value, modifiers }, vnode) {
    try {
      el.addEventListener('keydown', function(event) {

        if (event.keyCode == 32||event.which==32) {
          event.returnValue = false
        }
      });
    } catch (e) {
      console.log(e)
    }
  }
});

Vue.directive('elScroll', {
  update(el, binding) {
    try {
      let fixed=el.querySelector('.el-table__fixed-right');
      if (fixed) {
        binding.value(fixed)
      }

      // elTable.addEventListener('scroll', function(event) {
      //
      //   let $th = $(el).find('.custom-head-column');
      //   console.info(binding.value(fixed, $th));
      // });
    } catch (e) {
      console.log(e)
    }
  }
});

Vue.directive('d3Scroll', {
  bind(el, binding) {
    try {
      binding.value(el);
    } catch (e) {
      console.log(e)
    }
  }
});




Vue.directive('customIcon', {
  bind(el, { value, modifiers }, vnode) {
    // ImageToBase64.getBase64(value).then((base) => {
    //   el.style.backgroundImage='url('+base+')';
    // })
    el.style.backgroundImage='url('+value+')';
  },
  inserted(el, { value, modifiers }, vnode) {
    let isSelect = getParents(el, 'is-selected');
    if (isSelect.length!=0) {
      el.style.backgroundImage='url('+value+')';
    }
  },
  update(el, { value, modifiers }, vnode) {
    let isSelect = getParents(el, 'is-selected');
    if (isSelect.length!=0) {
      el.style.backgroundImage='url('+value+')';
    }
  }
});

/**
 * 选择会话存储位置localStorage/sessionStorage
 */
store.dispatch('changeStorageType', 'sessionStorage');


let  router;
let  App

RemoteModule.Load().then((r) => {
	if (isMobile) {
	    console.info('mobile');
	    import('@/mobile.css');
	    router = require('@/router/mobile');
	    App = require('@/Mobile');
	} else {
	    console.info('pc')
	    router = require('@/router/index');
	    App = require('@/App');
	}

	new Vue({
	    router: router.default,
	    store,
	    render: h => h(App.default)
	}).$mount('#app-main');
})

