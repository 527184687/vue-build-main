
export default {
  axioCancel: '',
  namespaced: true,
  state: {
    cancels: {}
  },
  mutations: {
    set(state, params) {
      state.cancels[`${params.method}_${params.url}_${params.params}`] = params.cancel;
    },
    off(state, params) {
      if (state.cancels[`${params.method}_${params.url}_${params.params}`]) {
        state.cancels[`${params.method}_${params.url}_${params.params}`](params);
        delete state.cancels[`${params.method}_${params.url}_${params.params}`];
      }
    },
    offAll(state, filter) {
      for (let k in state.cancels) {
        if (filter&&filter(k)) {
        } else {
          state.cancels[k]('取消请求:'+k);
          delete state.cancels[k];
        }
      }
    },
    rem(state, params) {
      delete state.cancels[`${params.method}_${params.url}_${params.params}`];
    }
  },
  actions: {
    offCancel(state, params) {
      state.commit('off', params);
    },
    offAllCancel(state, filter) {
      state.commit('offAll', filter);
    },
    removeCancel(state, params) {
      state.commit('rem', params);
    },
    pushCancel(state, params) {
      state.commit('set', params);
    }
  },
  getters: {

  }
}
