import DictionaryCode from './dictionaryCode'

const CustomFilters = {
  /**
     *
     * @param date
     * @param fmt
     * @returns {*}
     * @constructor
     */
  FormatDate(date, fmt) {
    if (!date) return '-'
    if (typeof fmt === 'undefined') {
      fmt = 'yyyy-MM-dd hh:mm:ss'
    }
    let time = new Date();
    if (typeof date === 'string') {
      time = new Date(date.replace(/-/g, '/').replace(/T|Z/g, ' ').trim());
    } else if (typeof date === 'object') {
      time = new Date(date);
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        (time.getFullYear() + '').substr(4 - RegExp.$1.length)
      )
    }
    var o = {
      'M+': time.getMonth() + 1, // 月份
      'd+': time.getDate(), // 日
      'h+': time.getHours(), // 小时
      'm+': time.getMinutes(), // 分
      's+': time.getSeconds(), // 秒
      'q+': Math.floor((time.getMonth() + 3) / 3), // 季度
      'S': time.getMilliseconds() // 毫秒
    };
    for (const k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        const str = o[k] + '';
        fmt = fmt.replace(
          RegExp.$1,
          RegExp.$1.length === 1 ? str : ('00' + str).substr(str.length)
        )
      }
    }
    return fmt
  },
  /**
     * 持续时间 天,时，分，秒
     * @param long
     * @param fmt d天h小时m分钟s秒S毫秒
     * @returns {*}
     * @constructor{
     *     day, hour, minute, second
     * }
     */
  FormatContinueTime(s, fmt) {
    if (s===null) return '-'
    // if (typeof fmt === 'undefined') {
    //   fmt = 'd天h小时m分钟s秒S毫秒'
    // }
    let Milliseconds= 1000, // 每秒1000毫秒
      SecondsPerMinute = 60, // 每分钟60秒
      SecondsPerHour = SecondsPerMinute * 60, // 每小时的秒
      SecondsPerDay = SecondsPerHour * 24, // 每天的秒
      day = 0, hour = 0, minute = 0, second = 0, milliseconds=0;


    milliseconds = s%Milliseconds;
    s = parseInt(s/Milliseconds);

    if (s > 60) {
      second = s % 60;
      minute = parseInt(s / SecondsPerMinute);
      if (minute > 60) {
        minute = minute % 60;
        hour = parseInt(s / SecondsPerHour);
        if (hour > 24) {
          hour = hour % 24;
          day = parseInt(s / SecondsPerDay)
        }
      }
    } else {
      second = s
    }

    let o = {
      'd+': {
        v: day,
        n: '天'
      }, // 日
      'h+': {
        v: hour,
        n: '小时'
      }, // 小时
      'm+': {
        v: minute,
        n: '分钟'
      }, // 分
      's+': {
        v: second,
        n: '秒'
      }, // 秒
      'S': {
        v: milliseconds,
        n: '毫秒'
      } // 毫秒
    };
    if (typeof fmt === 'undefined') {
      fmt ='';
      for (const k in o) {
        if (o[k].v) {
          fmt +=  o[k].v + o[k].n;
        }
      }
    } else {
      for (const k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
          const str = o[k].v + '';
          fmt = fmt.replace(
            RegExp.$1,
            RegExp.$1.length === 1 ? str : ('00' + str).substr(str.length)
          )
        }
      }
    }

    return fmt;
  },
  /**
     * 流量单位转换
     * sizes 为全单位从byte开始计算。开始从哪里计算把索引单位前移
     * */
  FormatFlowUnit(bytes) { // 流量转换
    if (!bytes) {
      return '0.0B'
    }
    if (!isNaN(parseInt(bytes))) {
      var k = 1024;
      var sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
      // var sizes = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
      var i = Math.floor(Math.log(Math.abs(bytes)) / Math.log(k));
      var ret = (bytes / Math.pow(k, i)).toFixed(2) + sizes[i];
      if (i === 0) {
        ret = bytes + sizes[i]
      }
      return ret
    } else {
      return '-'
    }
  },
  FormatFlowUnitkb(bytes) { // 流量转换
    if (!bytes) {
      return '0.0B'
    }
    const num = parseInt(bytes);
    if (!isNaN(num)) {
      if (num >= 1024) {
        return (num / (1024)).toFixed(2) + 'KB'
      } else {
        return (num.toFixed(2)) + 'B'
      }

    } else {
      return '-'
    }
  },

  // 涨幅样式
  /**
     * @return {string}
     */
  FormatGainsCss(val) {
    if (val === 0 || val === undefined || !val) {
      return '<span>0</span>'
    }
    if (val < 0) {
      return '<span  class="fall el-icon-caret-bottom">' + Math.abs(val) + '%</span>'
    } else {
      return '<span  class="rose el-icon-caret-top">' + val + '%</span>'
    }
    // if (val > 0 && val < 300) {
    //   return '<span  class="rose el-icon-caret-top">' + val + '%</span>';
    // } else if (val < 0) {
    //   return '<span  class="fall el-icon-caret-bottom">' + val + '%</span>';
    // } else if (val >= 300) {
    //   return '<span  class="rose el-icon-caret-top">300%+</span>';
    // }
  },

  FormatTdate(date) {
    if (date == null || date == '') {
      return '-'
    }
    return date.replace(/T/g, ' ')
  },

  FormatNum(val) {
    if (val == undefined || val == '-') {
      return '-'
    }

    const num = Number(val);
    if (num > 10000 * 10000 * 1000) {
      return (num / (10000 * 10000 * 1000)).toFixed(2) + '千亿'
    }
    if (num > 10000 * 10000 * 100) {
      return (num / (10000 * 10000 * 100)).toFixed(2) + '百亿'
    }
    if (num > 10000 * 10000) {
      return (num / (10000 * 10000)).toFixed(2) + '亿'
    }
    if (num > 10000 * 1000) {
      return (num / (10000 * 1000)).toFixed(2) + '千万'
    }
    if (num > 10000 * 100) {
      return (num / (10000 * 100)).toFixed(2) + '百万'
    }
    if (num > 10000) {
      return (num / 10000).toFixed(2) + '万'
    }
    return num
  },

  /**
     * @return {string}
     */
  DictionaryCodeToName(data, type, separator = ';') {
    if (DictionaryCode.value[type] && data != null) {
      if (data.constructor !== Array) {
        const D = DictionaryCode.value[type].find(d => {
          return d.value === data
        });
        return D ? D.label : '-'
      } else {
        var strs = [];
        let list = DictionaryCode.value[type];
        for (let i = 0; i < data.length; i++) {
          let o = list.find(d => {
            return d.value === data[i]
          });
          strs.push(o.label)
        }
        return strs.join(separator);
      }
    }
    return '-'
  },

  FormatBoolean(data) {
    if (data == '1') {
      return '是'
    }
    return '否'
  },
  // 圆点
  /**
     * @return {string}
     */
  FormatStatusCircle(val) {
    let html='';
    switch (val) {
    case 0:
      html='<span class="status-circle blue"></span>';
      break;
    case 1:
      html='<span class="status-circle yellow"></span>';
      break;
    case 2:
      html='<span class="status-circle red"></span>';
      break;
    }
    return html;
    // if (val > 0 && val < 300) {
    //   return '<span  class="rose el-icon-caret-top">' + val + '%</span>';
    // } else if (val < 0) {
    //   return '<span  class="fall el-icon-caret-bottom">' + val + '%</span>';
    // } else if (val >= 300) {
    //   return '<span  class="rose el-icon-caret-top">300%+</span>';
    // }
  },

}


CustomFilters.install = function(Vue) {
  Object.keys(CustomFilters).forEach(key => {
    Vue.filter(key, CustomFilters[key])// 插入过滤器名和对应方法
  });
  Vue.prototype.$filter = CustomFilters;
  window.$filter = CustomFilters;
}

export default CustomFilters;
