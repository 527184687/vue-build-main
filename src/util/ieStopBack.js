
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function() {
  history.pushState(null, null, document.URL);
});

function stopIt(ev) {
  if (ev.preventDefault) {
    // preventDefault()方法阻止元素发生默认的行为
    ev.preventDefault()
  }
  if (ev.returnValue) {
    // IE浏览器下用window.event.returnValue = false;实现阻止元素发生默认的行为
    window.event.returnValue = false;
  }
  return false
}
export function stopBackSpace(e) {
  let ev = e || window.event
  // 各种浏览器下获取事件对象
  let obj = ev.relatedTarget || ev.srcElement || ev.target || ev.currentTarget
  // 按下Backspace键
  if (ev.keyCode === 8) {
    // 标签名称
    let tagName = obj.nodeName.toLowerCase();
    // 如果标签不是input或者textarea则阻止Backspace
    if (tagName !== 'input' && tagName !== 'textarea') {
      return stopIt(ev)
    }
    let tagType = obj.type.toLowerCase() // 标签类型
    // input标签除了下面几种类型，全部阻止Backspace
    if (tagName === 'input' && (tagType !== 'text' && tagType !== 'textarea' && tagType !== 'password'&& tagType !== 'number')) {
      return stopIt(ev)
    }
    // input或者textarea输入框如果不可编辑则阻止Backspace
    if ((tagName === 'input' || tagName === 'textarea') && (obj.readOnly === true || obj.disabled === true)) {
      return stopIt(ev)
    }

  }

}
