const Rules = {
    /*
    * ip验证
    * */
    IP: (rule, value, callback) => {
        // if (value === '') {
        //   callback(new Error('请输入正确IP地址'));
        // }
        let ok = true;
        var ips = value.split(','),

            ip = /^((\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5]))(\/(\d{1,2}|1\d\d|2[0-4]\d|25[0-5]))?$/;
        for (var i = 0; i < ips.length; i++) {
            if (!ip.test(ips[i])) {
                ok = false;
                break;
            }
        }
        if (ok) {
            callback();
        } else {
            callback(new Error('IP地址不正确'));
        }
    },

    IPV6: (rule, value, callback) => {
        if (value === '') {
            callback(new Error('请输入正确IPv6地址'));
        }
        let ok = true;
        var ips = value.split(','),

            ip = /^([\da-fA-F]{1,4}:){6}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$|^::([\da-fA-F]{1,4}:){0,4}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$|^([\da-fA-F]{1,4}:):([\da-fA-F]{1,4}:){0,3}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$|^([\da-fA-F]{1,4}:){2}:([\da-fA-F]{1,4}:){0,2}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$|^([\da-fA-F]{1,4}:){3}:([\da-fA-F]{1,4}:){0,1}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$|^([\da-fA-F]{1,4}:){4}:((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$|^([\da-fA-F]{1,4}:){7}[\da-fA-F]{1,4}$|^:((:[\da-fA-F]{1,4}){1,6}|:)$|^[\da-fA-F]{1,4}:((:[\da-fA-F]{1,4}){1,5}|:)$|^([\da-fA-F]{1,4}:){2}((:[\da-fA-F]{1,4}){1,4}|:)$|^([\da-fA-F]{1,4}:){3}((:[\da-fA-F]{1,4}){1,3}|:)$|^([\da-fA-F]{1,4}:){4}((:[\da-fA-F]{1,4}){1,2}|:)$|^([\da-fA-F]{1,4}:){5}:([\da-fA-F]{1,4})?$|^([\da-fA-F]{1,4}:){6}:$/;
        for (var i = 0; i < ips.length; i++) {
            if (!ip.test(ips[i])) {
                ok = false;
                break;
            }
        }
        if (ok) {
            callback();
        } else {
            callback(new Error('IPv6地址不正确'));
        }
    },

    IPV6Mask: (rule, value, callback) => {
        if (value === '') {
            callback(new Error('请输入正确IP地址'));
        }
        let ok = true;
        var ips = value.split(','),

            ip = /^([\da-fA-F]{1,4}:){6}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^::([\da-fA-F]{1,4}:){0,4}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:):([\da-fA-F]{1,4}:){0,3}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){2}:([\da-fA-F]{1,4}:){0,2}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){3}:([\da-fA-F]{1,4}:){0,1}((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){4}:((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){7}[\da-fA-F]{1,4}(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^:((:[\da-fA-F]{1,4}){1,6}|:)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^[\da-fA-F]{1,4}:((:[\da-fA-F]{1,4}){1,5}|:)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){2}((:[\da-fA-F]{1,4}){1,4}|:)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){3}((:[\da-fA-F]{1,4}){1,3}|:)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){4}((:[\da-fA-F]{1,4}){1,2}|:)(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){5}:([\da-fA-F]{1,4})?(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$|^([\da-fA-F]{1,4}:){6}:(\/([1-9]?\d|(1([0-1]\d|2[0-8]))))?$/;
        for (var i = 0; i < ips.length; i++) {
            if (!ip.test(ips[i])) {
                ok = false;
                break;
            }
        }
        if (ok) {
            callback();
        } else {
            callback(new Error('IP地址不正确'));
        }
    },

    Mask: (rule, value, callback) => {
        var t = /([1-9]?\d|(1([0-1]\d|2[0-8])))/;
        if (value === '') {
            callback(new Error('请输入前缀'));
        }
        if (t.test(value)) {
            callback();
        } else {
            callback(new Error('请输入前缀'));
        }
    },

    /*
    * port验证
    * */
    Port: (rule, value, callback) => {
        const reg = /^[0-9]*[1-9][0-9]*$/;
        if (value === '') {
            callback(new Error('请输入正确端口'));
        }
        if (!reg.test(value)) {
            return callback(new Error('请输入整数'))
        } else if (value >= 0 && value <= 65535) {
            callback();
        } else {
            callback(new Error('端口范围0-65535'));
        }
    },

    /*
    * 整数正则
    * */
    checkInterNum: (rule, value, callback) => {
        const reg = /^[0-9]*[1-9][0-9]*$/;
        if (!reg.test(value)) {
            return callback(new Error('请输入正确数值'))
        } else {
            callback()
        }
    },
    /*
    * 整数正则包括0
    * */
    checkInterNum1: (rule, value, callback) => {
        const reg = /^[0-9]*[0-9][0-9]*$/;
        if (!reg.test(value)) {
            return callback(new Error('请输入正确数值'))
        } else {
            callback()
        }
    },
    /*
    * URL正则
    * */
    isURL: (rule, value, callback) => {

        var reg = new RegExp(
            '^' +
            // protocol identifier
            '(?:(?:https?|ftp)://)' +
            // user:pass authentication
            '(?:\\S+(?::\\S*)?@)?' +
            '(?:' +
            // IP address dotted notation octets
            // excludes loopback network 0.0.0.0
            // excludes reserved space >= 224.0.0.0
            // excludes network & broacast addresses
            // (first & last IP address of each class)
            '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
            '(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
            '(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
            '|' +
            // host name
            '(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)' +
            // domain name
            '(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*' +
            // TLD identifier
            '(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))' +
            ')' +
            // port number
            '(?::\\d{2,5})?' +
            // resource path
            '(?:/\\S*)?' +
            '$', 'i'
        );
        if (!value) {
            return callback(new Error('请输入网页地址'))
        } else if (!reg.test(value)) {
            return callback(new Error('网页地址不正确,例:<scheme>://<host>:<port>/<path>'))
        } else {
            callback()
        }
    },

    /*
    * 电话验证
    * */
    isPhone: (rule, value, callback) => {
        if (!value) {
            return callback();
        }
        var pattern = /^(0\d{2,3}-\d{7,8})|(1[34578]\d{9})$/;
        if (pattern.test(value)) {
            return callback()
        }
        return callback(new Error('输入的手机号错误'))
    },

// 4-16个字符，必须字母开头，字母数字下划线
    validUserName: (rule, value, callback) => {
        const reg = /^[a-zA-Z][a-zA-Z0-9_]*$/
        // /^[a-zA-Z0-9_\u4e00-\u9fa5]{5,20}$/
        if (reg.test(value)) {
            callback()
        } else {
            return callback(new Error('请输入正确格式的名称！'))
        }
    },

    /* 密码  8-32位,应至少包含数字、大小写字母及特殊字符中的两种*/
    validPass: (rule, value, callback) => {
        const Reg = /^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)])+$).{8,32}$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('请输入正确格式的密码'))
        }
    },

    validPassback: (rule, value, callback) => {
        const Reg = /^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)])+$).{8,32}$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('密码错误,请重新输入！'))
        }
    },
// 数据库名 1-128个字符，字母数字下划线，特殊字符支持`~!@#$%^&*()-_=+\|[{}];:'",<.>/?
    validDatabase: (rule, value, callback) => {
        // const Reg = /^([`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]]){1,128}$/
        const Reg = /^[a-zA-Z0-9_`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]{1,128}$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('请输入正确格式的数据库名'))
        }
    },

// 联系人 2-20个字符
    validContact: (rule, value, callback) => {
        const Reg = /^[\u4e00-\u9fa5]{2,20}$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('请输入2-20个汉字'))
        }
    },

// 用户名(1-100个字符!)特殊字符支持`~!@#$%^&*()-_=+\|[{}];:'",<.>/?
    validUser: (rule, value, callback) => {
        const Reg = /^([a-zA-Z0-9_`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]){1,100}$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('请输入正确格式的用户名'))
        }
    },
// 数据库用户名密码(1-100个字符!)特殊字符支持`~!@#$%^&*()-_=+\|[{}];:'",<.>/?
    validUserpass: (rule, value, callback) => {
        const Reg = /^([a-zA-Z0-9_`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]){1,100}$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('请输入正确格式的密码'))
        }
    },
// 资源名称(4-16个字符，字母、数字、汉字、特殊字符支持`~!@#$%^&*()-_=+\|[{}];:'",<.>/?
    validResourceName: (rule, value, callback) => {
        const Reg = /^[a-zA-Z0-9\u4e00-\u9fa5_`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]{4,16}$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('请输入正确格式的资源名称'))
        }
    },
// 联系人 2-20个字符
    validdataContact: (rule, value, callback) => {
        //  if (!value) {
        //    return callback()
        //  }
        const Reg = /^[\u4e00-\u9fa5]{2,20}$/
        if (Reg.test(value) || value == '') {
            callback()
        } else {
            callback(new Error('请输入2-20个汉字'))
        }
    },
// 备注(1-100个字符，字母、数字、汉字、特殊字符支持`~!@#$%^&*()-_=+\|[{}];:'",<.>/?
    validRemark: (rule, value, callback) => {
        const Reg = /^([a-zA-Z0-9\u4e00-\u9fa5_`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]){1,100}$/
        if (Reg.test(value) || value == '') {
            callback()
        } else {
            callback(new Error('请输入正确格式的备注'))
        }
    },

// 特殊字符支持`~!@#$%^&*()-_=+\|[{}];:'",<.>/?
    validSpecial: (rule, value, callback) => {
        const Reg = /^[a-zA-Z0-9_`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]*$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('输入格式不正确'))
        }
    },
// 支持中文，特殊字符支持`~!@#$%^&*()-_=+\|[{}];:'",<.>/?
    validChinaSpecial: (rule, value, callback) => {
        const Reg = /^[a-zA-Z0-9\u4e00-\u9fa5_`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]*$/
        if (Reg.test(value)) {
            callback()
        } else {
            callback(new Error('输入格式不正确'))
        }
    },
// 4-16个字符，必须字母开头，字母数字下划线
    validUpperSpecial: (rule, value, callback) => {
        const reg = /^[a-zA-Z][a-zA-Z0-9_`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]*$/
        // /^[a-zA-Z0-9_\u4e00-\u9fa5]{5,20}$/
        if (reg.test(value)) {
            callback()
        } else {
            return callback(new Error('输入格式不正确！'))
        }
    },
// 密码长度为 8 到 32 个字符,需要包含大写、小写、数字和特殊字符至少三种!
    validatePass: (rule, value, callback) => {
        let reg = /^(?![a-zA-Z]+$)(?![A-Z0-9]+$)(?![A-Z\\W`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]+$)(?![a-z0-9]+$)(?![a-z\\W`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]+$)(?![0-9\\W`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]+$)[a-zA-Z0-9\\W`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?-]*$/;
        if (reg.test(value)) {
            callback();
        } else {
            callback(new Error('输入格式不正确!'));
        }
    },

// 密码长度8-32个字符，至少包含字母、数字、特殊字符支持`~!@#$%^&*()-_=+\|[{}];:'",<.>/?中的任两种
    validateloginPass: (rule, value, callback) => {
        if (!(/[A-Z]/.test(value) && /[a-z]/.test(value))) {
            return callback(new Error('请输入正确格式密码'))
        }
        const Reg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?]+$)[\da-zA-Z`~!@#\$%\^&\*\(\)_=\+\\|\[\{\}\];:'",<.>\/\?]{8,32}$/
        if (!value) {
            return callback(new Error('请输入密码'))
        }
        if (!Reg.test(value)) {
            callback(new Error('请输入正确格式的密码'))
        } else {
            callback()
        }
    },

// 身份证正则 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
    isCardNo: (rule, value, callback) => {
        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if (reg.test(value)) {
            callback();
        } else {
            callback(new Error('身份证输入不合法!'));
        }
    }
    ,
// 邮件服务器地址校验
    isEmailServer: (rule, value, callback) => {
        var reg = /(^((pop3|smtp|smtp.vip|pop.vip)\.|@)\w+\.(com|cn|net)$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$)|(^((2(5[0-5]|[0-4]\d))|[0-1]?\d{1,2})(\.((2(5[0-5]|[0-4]\d))|[0-1]?\d{1,2})){3}$)/;
        if (reg.test(value)) {
            callback();
        } else {
            callback(new Error('邮件服务器地址不合法!'));
        }
    }
    ,
    validresName: (rule, value, callback) => {
        console.log(value)
        const reg = /^[\s\u4e00-\u9fa5]{1,20}$/
        const val = value
        if (val == '') {
            callback(new Error('请输入姓名'))
        }
        if (!reg.test(val)) {
            callback(new Error('不超过20个字，支持汉字'))
        } else {
            callback()
        }
    },
    validresPhone: (rule, value, callback) => {
        const phoneReg = /^1[3|4|5|6|7|8][0-9]{9}$/
        const val = value
        if (!val) {
            return callback(new Error('请输入手机号码'))
        }
        if (phoneReg.test(val)) {
            callback()
        } else {
            callback(new Error('请输入正确格式的手机号码'))
        }
    },
    validreSmail: (rule, value, callback) => {
        // eslint-disable-line
        const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
        if (value == '') {
            callback()
        } else if (!reg.test(value)) {
            callback(new Error('请输入正确的邮箱格式'))
        } else {
            callback()
        }
    },
    validresgeneral: (rule, value, min, max, callback) => {
        const reg = new RegExp('^[0-9a-zA-Z_\u4e00-\u9fa5]{' + min + ',' + max + '}$');
        if (!reg.test(value)) {
            // callback(new Error(min + '-' + max + '个字符,支持汉字、字母、数字、下划线'));
            callback(new Error('请输入正确的格式'));
        } else {
            callback();
        }
    },
    validresgeneralmid: (rule, value, min, max, callback) => {
        const reg = new RegExp('^[\-0-9a-zA-Z_\u4e00-\u9fa5]{' + min + ',' + max + '}$');
        if (!reg.test(value)) {
            // callback(new Error(min+'-' + max + '个字符,支持汉字、字母、数字、下划线，中划线'));
            callback(new Error('请输入正确的格式'));
        } else {
            callback();
        }
    },
    validresMask: (rule, value, callback) => {
        const reg = new RegExp('^[0-9a-zA-Z_\u4e00-\u9fa5]{1,200}$');
        if (!reg.test(value) && value != '') {
            callback(new Error('不超过200个字,支持汉字、字母、数字、下划线'));
        } else {
            callback();
        }
    }
}
