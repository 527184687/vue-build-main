var scale =1;
var maxSize = 1000*1000;

function getBase64(file) {
  return new Promise(function(resolve, reject) {
    const reader = new FileReader();
    let imgResult = '';

    reader.readAsDataURL(file);
    reader.onload = function() {
      imgResult = reader.result;
      var img = new ImageObject();
      img.src = imgResult;

      img.onload = function() {
        // compressImg(imgResult);
        var size = file.size;
        if (size>maxSize) {
          scale = maxSize/size;
        }
        var img_width = this.width;
        var img_height = this.height;
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var final_width =img_width *scale;
        var final_height = img_height *scale;
        var cwa = document.createAttribute('width');
        cwa.nodeValue = final_width;
        canvas.setAttributeNode(cwa);
        var cwh = document.createAttribute('height');
        cwh.nodeValue = final_height;
        canvas.setAttributeNode(cwh);
        ctx.drawImage(this, 0, 0, final_width, final_height);
        imgResult = canvas.toDataURL('image/jpeg');
        // document.getElementById('img2').src = imgResult;
        var blob = convertBase64UrlToBlob(imgResult);
        // 计算图片压缩比
        console.log('原大小：' + size);
        console.log('现大小：' + blob.size);
        var rate = ((blob.size / size) * 100).toFixed(2);
        console.log('压缩率：' + rate + '%');
        resolve(imgResult)
      }

    }
    reader.onerror = function(error) {
      reject(error)
    }
    reader.onloadend = function() {

    }
  }
  )
}


function convertBase64UrlToBlob(urlData) {
  var arr = urlData.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
}

function ImgToBase64(src) {
  return new Promise(function(resolve, reject) {
    let img = new ImageObject();
    img.src = src;
    img.onload = function() {
      let img_width = this.width;
      let img_height = this.height;
      let canvas = document.createElement('canvas');
      canvas.style.backgroundColor='transparent';
      let ctx = canvas.getContext('2d');
      let final_width =img_width *scale;
      let final_height = img_height *scale;
      let cwa = document.createAttribute('width');
      cwa.nodeValue = final_width;
      canvas.setAttributeNode(cwa);
      let cwh = document.createAttribute('height');
      cwh.nodeValue = final_height;
      canvas.setAttributeNode(cwh);
      ctx.drawImage(this, 0, 0, final_width, final_height);
      let imgResult = canvas.toDataURL('image/png');
      // document.getElementById('img2').src = imgResult;
      let blob = convertBase64UrlToBlob(imgResult);
      // // 计算图片压缩比
      // console.log('原大小：' + size);
      // console.log('现大小：' + blob.size);
      // let rate = ((blob.size / size) * 100).toFixed(2);
      // console.log('压缩率：' + rate + '%');
      resolve(imgResult)
    }
  })

}

async function image2Base64(file) {
  let result = await getBase64(file);
  return result;
}


export default {
  to: image2Base64,
  getBase64: ImgToBase64
}
