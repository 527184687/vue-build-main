/** *重写element-ui $message 解决消息多次弹出的问题**/
// import { MessageBox } from 'element-ui';

const confirmMessage = (message, title, options) => {
  if (typeof title === 'object' && !options) {
    title = Object.assign(title, {
      closeOnClickModal: false,
      closeOnPressEscape: false,
      closeOnHashChange: false,
      showClose: false,
      cancelButtonClass: 'xl-cancel',
      confirmButtonClass: 'xl-confirm'
    })
  }

  return MessageBox.confirm(message, title, options);
};

export default confirmMessage;

