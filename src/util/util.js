/**
 * Util工具函数
 */
var Util = {};
// :options="{animation:5,preventOnFilter:false,scroll:true,scrollFn:dragScro,scrollSensitivity:0,scrollSpeed:0}"
Util.check = {
  // 判断手机号输入是否是有效
  isPhone(str) {
    var reg = /(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/;
    return reg.test(str);
  },
  // 判断邮箱输入是否是有效
  isEmail(str) {
    return /^\w+([-\.]\w+)*@\w+([\.-]\w+)*\.\w{2,4}$/.test(str);
  }
};

// Date格式化函数
Util.format = function(date, fmt) {
  date = new Date(date);
  var o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours() % 12 == 0 ? 12 : date.getHours() % 12,
    'H+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds(),
    'q+': Math.floor((date.getMonth() + 3) / 3),
    S: date.getMilliseconds()
  };
  var week = {
    '0': '\u65e5',
    '1': '\u4e00',
    '2': '\u4e8c',
    '3': '\u4e09',
    '4': '\u56db',
    '5': '\u4e94',
    '6': '\u516d'
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(
      RegExp.$1,
      (date.getFullYear() + '').substr(4 - RegExp.$1.length)
    );
  }
  if (/(E+)/.test(fmt)) {
    fmt = fmt.replace(
      RegExp.$1,
      (RegExp.$1.length > 1 ?
        RegExp.$1.length > 2 ?
          '\u661f\u671f' :
          '\u5468' :
        '') + week[date.getDay() + '']
    );
  }
  for (var k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length)
      );
    }
  }
  return fmt;
};

/*
 * JSON数组去重
 * @param: [array] json Array
 * @param: [string] 唯一的key名，根据此键名进行去重
 */
Util.uniqueArray = function(array, key) {
  var result = [array[0]];
  for (var i = 1; i < array.length; i++) {
    var item = array[i];
    var repeat = false;
    for (var j = 0; j < result.length; j++) {
      if (item[key] == result[j][key]) {
        repeat = true;
        break;
      }
    }
    if (!repeat) {
      result.push(item);
    }
  }
  return result;
};
/**
 * 防抖函数
 * @param method 事件触发的操作
 * @param delay 多少毫秒内连续触发事件，不会执行
 * @returns {Function}
 */
Util.debounce = function(method, delay) {
  let timer = null;
  // let self = this,
  //         args = arguments;
  return function(method, delay) {
    let self = this,
      args = arguments;
    timer && clearTimeout(timer);
    timer = setTimeout(function() {
      method.apply(self, args);
    }, delay);
  };
};
/**
 * 求百分比
 * @param num 当前数
 * @param total 总数
 * @returns {string}
 */
Util.GetPercent = function(num, total) {
  num = parseFloat(num);
  total = parseFloat(total);
  if (isNaN(num) || isNaN(total)) {
    return '-';
  }
  return total <= 0 ? '0%' : Math.round((num / total) * 10000) / 100.0 + '%';
};
/********
 * 数组求和
 * @param arr 数组
 * @param key 数组key
 * @returns {number}
 */
Util.sum = function(arr, key) {
  var s = 0;
  for (var i = arr.length - 1; i >= 0; i--) {
    if (key) {
      s += parseInt(arr[i][key]);
    } else {
      s += parseInt(arr[i]);
    }
  }
  return s;
};
Util.isIE = function() {
  if (!!window.ActiveXObject || 'ActiveXObject' in window) {
    return true;
  } else {
    return false;
  }
}
export default Util;
