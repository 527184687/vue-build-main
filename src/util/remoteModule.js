import ComsUtil from '@/util/components';
import axiosInstance from '../lib/request';

const RemoteModule = {
  Load(menu) {
    const CommonModule = {
      '@/common/CuDatepicker': require('@/common/CuDatepicker'),
      '@/common/CuDrawer': require('@/common/CuDrawer'),
      '@/common/CuPopover': require('@/common/CuPopover'),
      '@/common/CustomPage': require('@/common/CustomPage'),
      '@/common/CustomPageMin': require('@/common/CustomPageMin'),
      '@/common/DynamicForm': require('@/common/DynamicForm'),
      '@/common/DynamicTable': require('@/common/DynamicTable'),
      '@/common/HTMLselect': require('@/common/HTMLselect'),
      '@/common/TreeSelect': require('@/common/TreeSelect'),
      '@/common/UploadDialog': require('@/common/UploadDialog'),
      '@/common/UploadInput': require('@/common/UploadInput'),
      '@/lib/public-method': require('@/lib/public-method'),
    };

    for (let k in CommonModule) {
      window[k] = global[k] = CommonModule[k]
    }
    var data = [{
      'path': 'Page',
      'version': 'v0.0.1',
      'componentCss': 'http://localhost:8802/module/static/lib/Page/@v0.0.1/Page.css',
      'component': 'http://localhost:8802/module/static/lib/Page/@v0.0.1/Page',
      'menuType': 'L'
    }, {
      'path': 'Search',
      'version': 'v0.0.1',
      'componentCss': 'module/Search.css',
      'component': 'module/Search.umd.js',
      'menuType': 'M'
    },
    {
      'path': 'Investigate',
      'version': 'v0.0.1',
      'componentCss': 'module/Investigate.css',
      'component': 'module/Investigate.umd.js',
      'menuType': 'M'
    }];
    // return new Promise((resolve) => {
    //   ComsUtil.remoteRequire(data);
    //   resolve(true);
    // });
    return new Promise((resolve) => {
      axiosInstance({
        url: '/module/list',
        method: 'get',
      }).then((result) => {
        let { data, code, message } = result.data;
        console.log('请求资源', data);
        ComsUtil.remoteRequire(data);
        resolve(true);
      })
    });
  }
};

export default RemoteModule;
