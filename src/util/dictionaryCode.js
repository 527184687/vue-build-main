import axiosInstance from '@/lib/request';

window.IsObjectValueEqual=function(a, b) {
  var aProps = Object.getOwnPropertyNames(a);
  var bProps = Object.getOwnPropertyNames(b);
  // if (aProps.length != bProps.length) {
  //   return false;
  // }
  for (var i = 0; i < aProps.length; i++) {
    var propName = aProps[i]
    if (propName==='__ob__') continue;
    var propA = a[propName]
    var propB = b[propName]
    if (!b.hasOwnProperty(propName)) return false
    if ((propA instanceof Object)) {
      if (IsObjectValueEqual(propA, propB)) {
        // return true     这里不能return ,后面的对象还没判断
      } else {
        return false
      }
    } else if (propA !== propB) {
      return false
    } else { }
  }
  return true
}
// 差集
Array.prototype.minus = function(list, filter) {
  var obj = {
    list: this.concat(),
    noupdata: [],
    update: [],
    newlist: []
  };
  for (let i = 0; i < list.length; i++) {
    let target = list[i];
    var include = true;
    for (let j = 0; j < this.length; j++) {
      let source = this[j];
      if (filter(source, target)) {
        include = false;
        break;
      }
    }
    if (include) {
      obj.newlist.push(target);
    }
  }
  for (var i = 0; i < this.length; i++) {
    let source = this[i];

    let isExist = false;
    for (let j = 0; j < list.length; j++) {
      let target = list[j];
      if (filter(source, target)) {
        obj.update.push({
          index: i,
          newobj: target
        });
        obj.list.splice(i, 1, target);
        isExist = true;
        break;
      }
    }
    if (!isExist) {
      obj.noupdata.push({
        index: i,
        source
      });
    } else {

    }
  }
  return obj;
};

Date.prototype.format = function(fmt) { // author: meizz
  var o = {
    'M+': this.getMonth() + 1, // 月份
    'd+': this.getDate(), // 日
    'h+': this.getHours(), // 小时
    'm+': this.getMinutes(), // 分
    's+': this.getSeconds(), // 秒
    'q+': Math.floor((this.getMonth() + 3) / 3), // 季度
    'S': this.getMilliseconds()
    // 毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 -
            RegExp.$1.length));
  for (var k in o)
    if (new RegExp('(' + k + ')').test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ?
        (o[k]) :
        (('00' + o[k]).substr(('' + o[k]).length)));
  return fmt;
};
if (!String.prototype.replaceAll) {
  String.prototype.replaceAll = function(s1, s2) {
    return this.replace(new RegExp(s1, 'gm'), s2);
  }
}

var DictionaryCode = {
  value: {},
  getByType(type) {
    if (Object.keys(this.value).length === 0) {
      console.info('未初始化');
      this.init();
    }
    return this.value[type] || [];
  },
  async init() {
    console.log('加载code..........................');
    var me = this;

    await getDictDataList();

    console.log('完成code..........................')
  }
};
const getDictDataList = async() => {
  // axiosInstance({
  //   url: '/system/sources/dictdata',
  //   method: 'get',
  // }).then(res => {
  //   if (res.data.code === 200) {
  //     let d = res.data.data;
  //     for (let k in d) {
  //       DictionaryCode.value[k] = d[k];
  //     }
  //
  //   }
  // })
}
const getFiletype = async() => {

}
DictionaryCode.install = function(Vue) {
  DictionaryCode.init();
  Vue.prototype.$code = DictionaryCode;
  window.$code = DictionaryCode;
};

export default DictionaryCode;
