// id随机数
export const getId = () =>
  Math.random()
    .toString(36)
    .substr(3);
// 起始和结束ip重复性校验
export const isRepeat = (arr) => {
  const hash = {};
  for (const i in arr) {
    const key = arr[i].start + arr[i].end;
    if (hash[key]) {
      return true;
    }
    hash[key] = true;
  }
  return false;
};
// 为空校验
export const isnull = (arr) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].start == '' || arr[i].end == '') {
      return true;
    }
  }
  return false;
};

export const completeIp = (ip) => {
  let ipV6 = ip;
  const index = ip.indexOf('::');
  if (index > 0) {
    // 123::在结尾
    if (index + 2 == ip.length) {
      const size = 8 - (ip.split(':').length - 1);
      let tmp = '';
      for (let i = 0; i < size; i++) {
        tmp += ':0';
      }
      tmp += ':0';
      ipV6 = ip.replace('::', tmp);
    } else {
      // 123：：123在中间
      const size = 8 - (ip.split(':').length - 1);
      let tmp = '';
      for (let i = 0; i < size; i++) {
        tmp += ':0';
      }
      tmp += ':';
      ipV6 = ip.replace('::', tmp);
    }
  } else if (index == 0) {
    if (ip.length == 2) {
      // 当ip为：：
      ipV6 = ip.replace('::', '0:0:0:0:0:0:0:0');
    } else {
      // 当ip为：：123
      ipV6 = ip.replace('::', '0:0:0:0:0:0:0:');
    }
  }
  return ipV6;
};
// 起始ip大于结束ip判断
export const compareIP = (ipBegin, ipEnd) => {
  let temp1;
  let temp2;

  if (ipBegin.indexOf('.') > 0 && ipEnd.indexOf('.') > 0) {
    temp1 = ipBegin.split('.');
    temp2 = ipEnd.split('.');

    for (let i = 0; i < 4; i++) {
      if (parseInt(temp1[i]) > parseInt(temp2[i])) {
        return 1;
      } else if (parseInt(temp1[i]) < parseInt(temp2[i])) {
        return -1;
      }
    }
    return 0;
  } else {
    // ipv6 起始大小对比

    temp1 = completeIp(ipBegin)
      .toUpperCase()
      .split(':');
    temp2 = completeIp(ipEnd)
      .toUpperCase()
      .split(':');

    for (let i = 0; i < temp1.length; i++) {
      if (temp1[i] == '') {
        if (temp2[i] == '') {
          continue;
        } else {
          return -1;
        }
      } else if (temp2[i] == '') {
        return 1;
      } else if (parseInt(temp1[i], 16) > parseInt(temp2[i], 16)) {
        return 1;
      } else if (parseInt(temp1[i], 16) < parseInt(temp2[i], 16)) {
        return -1;
      } else {
        continue;
      }
    }
    return 0;
  }
};

export const sameIP = (ipBegin, ipEnd) => {
  // 都是ipv4
  if (ipBegin.indexOf('.') > 0 && ipEnd.indexOf('.') > -1) {
    return -1;
  } else if (ipBegin.indexOf(':') > -1 && ipEnd.indexOf(':') > -1) {
    // ipv6
    return -1;
  } else if (ipBegin.indexOf('.') > 0 && ipEnd.indexOf(':') > -1) {
    // 起始ipv4 结束ipv6
    return 1;
  } else if (ipBegin.indexOf(':') > -1 && ipEnd.indexOf('.') > 0) {
    // 起始ipv6 结束ipv4
    return 1;
  }
};

// 判断Ipv4 ipv6
export const ipType = (ip) => {
  // 都是ipv4
  if (ip.indexOf('.') > 0) {
    return 1;
  } else if (ip.indexOf(':') > -1) {
    return 2;
  }
};
export const formatDate = (value) => {
  const date = new Date(value);
  const y = date.getFullYear();
  const MM =
    date.getMonth() + 1 < 10 ?
      '0' + (date.getMonth() + 1) :
      date.getMonth() + 1;
  const d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  const h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  const m =
    date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  const s =
    date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
  return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s;
};
