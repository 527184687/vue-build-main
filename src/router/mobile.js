import Vue from 'vue';
import Router from 'vue-router';
import Login from '../components/mobile/login/index';
import Home from '../components/mobile/home/index';
import MobileIndex from '../components/mobile/index';

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
};
Vue.use(Router);
const  router = new Router({
  mode: 'history',
  fallback: false,
  routes: [
    {
      path: '/default',
      name: 'Mobile',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      // 引导页面
      path: '/',
      name: 'Guide',
      component: MobileIndex
    },
    {
      path: '*',
      redirect: '/',
    },
  ]
});
router.beforeEach(async(to, from, next) => {
  if (to.path === '/login') {
    const user = window.sessionStorage.getItem('user')
    // if (user) {
    //   next('/default')
    // }
    next()
  } else {
    const user = window.sessionStorage.getItem('user');
    if (!user) {
      next('/login')
    }
    next()
  }
})
router.afterEach((to, from, nex) => {
  history.pushState(null, null, document.URL);
});
export default router;
