import Vue from 'vue';
import Router from 'vue-router';
import Home from '../components/home/index';
import IndexDefault from '../components/home/indexdefault';
import IndexAside from '../components/home/indexAside';
import Login from '../components/login/index';
import Guide from '../components/guide/index';

// 解决导航栏或者底部导航tabBar中的vue-router在3.0版本以上频繁点击菜单报错的问题。
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(Router);

const router = new Router({
  mode: 'history',
  fallback: false,
  routes: [
    {
      // 登录页面
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'Home',
      components: {
        default: Home
      }
    },
    {
      path: '/aside',
      name: 'Aside',
      components: {
        default: IndexAside
      }
    },
    {
      path: '/default',
      name: 'Default',
      components: {
        default: IndexDefault
      }
    },
    // {
    //   path: '/warningtopic',
    //   component: Layout,
    //   children: [
    //     {
    //       path: 'index',
    //       name: 'index',
    //       component: () => import('@/module/topic/index/index'),
    //       meta: { title: '专题中心' }
    //     },
    //     {
    //       path: 'config',
    //       name: 'config',
    //       component: () => import('@/module/topic/index/config'),
    //       meta: { title: '配置' }
    //     },
    //     {
    //       path: 'list',
    //       name: 'list',
    //       component: () => import('@/module/topic/index/list'),
    //       meta: { title: '预警信息' }
    //     }
    //   ]
    // },
    {
      // 引导页面
      path: '/',
      name: 'Guide',
      component: Guide
    },
    {
      path: '*',
      redirect: '/',
    },
  ]
});


router.beforeEach(async(to, from, next) => {
  if (to.path === '/login') {
    const user = window.localStorage.getItem('user')||window.sessionStorage.getItem('user')
    if (user) {
      next('/home')
    }
    next()
  } else {
    const user = window.localStorage.getItem('user')||window.sessionStorage.getItem('user');
    if (!user) {
      next('/login')
    }
    next()
  }
})
router.afterEach((to, from, nex) => {
  history.pushState(null, null, document.URL);
});

export default router
