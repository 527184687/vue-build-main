export default function(path) {
  return {
    handleBathDelete() {
      if (this.multipleSelection.length === 0) {
        this.$message.warning({
          showClose: true,
          message: '至少选择一条数据！',
        });
      } else {
        this.$confirm('确定删除这些资源吗？', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning',
        })
          .then(() => {
            let ids = [];
            this.multipleSelection.forEach(item => {
              ids.push(item);
            });
            this._delete(ids)
          })
          .catch(() => {
          });
      }
    },
    handleQuery() {
      this.pageNo = 1;
      this.loadData();
    },
    // 翻页
    handleSizeChange(size) {
      this.pageSize = size;
      this.loadData();
    },
    // 翻页
    handleCurrentChange(currentPage) {
      this.pageNo = currentPage;
      this.loadData();
    },
    handleSelectionChange(val) {
      this.multipleSelection = val;
    },
  }
}
