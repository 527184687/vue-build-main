var WaterMarker={
  addWaterMarker(dom, strs) {
    var can = document.createElement('canvas');
    var body = dom;
    body.appendChild(can);
    const rowH = 35;
    const h = rowH * strs.length;
    const ce = rowH / strs.length;// 每行高度

    can.style.display='none';
    var cans = can.getContext('2d');
    cans.font = '16px 宋体'; // 画布里面文字的字体
    can.width=cans.measureText(strs[0]).width*1.3; // 画布的宽
    can.height= h*1.4;// 画布的高度
    cans.translate((can.width/2), (can.height/2));
    cans.rotate(-25*Math.PI/180); // 画布里面文字的旋转角度
    cans.translate(-1 * (can.width/2), -1 * (can.height/2))

    cans.fillStyle = '#ccdfff';// 画布里面文字的颜色
    cans.textAlign = 'center';
    cans.textBaseline = 'middle';
    for (let i = 0; i < strs.length; i++) {
      cans.fillText(strs[i], can.width/2, can.height/2+(rowH * i-ce)); // 画布里面文字的间距比例
    }

    body.style.backgroundImage='url('+can.toDataURL('image/png')+')'; // 把画布插入到body中
  }
}
export default WaterMarker;
