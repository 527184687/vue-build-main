module.exports = {
  'compact': false,
  presets: ['@vue/cli-plugin-babel/preset'],
  ignore: ['./src/lib/js/mui.*']
};
