import ComsUtil from '@/util/components';

export default {
  Load: (menu) => {

    var components = [{
      'path': 'ResourceManage',
      'version': 'v0.0.1',
      'componentCss': 'http://127.0.0.1:8804/module/ResourceManage.css',
      'component': 'http://127.0.0.1:8804/module/ResourceManage.umd.js',
      'menuType': 'L'
    }, {
      'path': 'UserManage',
      'version': 'v0.0.1',
      'componentCss': 'http://127.0.0.1:8804/module/UserManage.css',
      'component': 'http://127.0.0.1:8804/module/UserManage.umd.js',
      'menuType': 'L'
    }, {
      'path': 'AuthManage',
      'version': 'v0.0.1',
      'componentCss': 'http://127.0.0.1:8804/static/lib/AuthManage/@v1.0.0/AuthManage.css',
      'component': 'http://127.0.0.1:8804/static/lib/AuthManage/@v1.0.0/AuthManage',
      'menuType': 'L'
    }, {
      'path': 'ServiceLogManage',
      'version': 'v0.0.1',
      'componentCss': 'http://127.0.0.1:8804/module/ServiceLogManage.css',
      'component': 'http://127.0.0.1:8804/module/ServiceLogManage.umd.js',
      'menuType': 'L'
    }];
    console.log('请求资源', components);
    ComsUtil.remoteRequire(components);


    // const CommonModule = {
    //   '@/common/CuDatepicker': require('@/common/CuDatepicker'),
    //   '@/common/CuDrawer': require('@/common/CuDrawer'),
    //   '@/common/CuPopover': require('@/common/CuPopover'),
    //   '@/common/CustomPage': require('@/common/CustomPage'),
    //   '@/common/CustomPageMin': require('@/common/CustomPageMin'),
    //   '@/common/DynamicForm': require('@/common/DynamicForm'),
    //   '@/common/DynamicTable': require('@/common/DynamicTable'),
    //   '@/common/HTMLselect': require('@/common/HTMLselect'),
    //   '@/common/TreeSelect': require('@/common/TreeSelect'),
    //   '@/common/UploadDialog': require('@/common/UploadDialog'),
    //   '@/common/UploadInput': require('@/common/UploadInput'),
    //   '@/lib/public-method': require('@/lib/public-method'),
    // };
    //
    // for (let k in CommonModule) {
    //   window[k] = global[k] = CommonModule[k]
    // }
  }
}
